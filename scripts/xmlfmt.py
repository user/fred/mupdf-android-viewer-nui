#!/bin/env python

import xml.etree.ElementTree as ET
import sys

out = sys.stdout

NS = '{http://schemas.android.com/apk/res/android}'
ANS = 'android:'

def escape(str):
	out = '"'
	for c in str:
		if c == '"': out += '&quot;'
		elif c == '<': out += '&lt;'
		elif c == '>': out += '&gt;'
		elif c == '&': out += '&amp;'
		elif ord(c) < 32: out += ('&#x%04X;' % ord(c))
		elif ord(c) > 127: out += ('&#x%04X;' % ord(c))
		else: out += c
	return out + '"'

def indent(level):
	out.write('\t' * level)

def attcmp(a, b):
	a = a.replace(NS, ANS)
	b = b.replace(NS, ANS)
	if a < b: return -1
	if a > b: return 1
	return 0

def dumptag(node, level):
	out.write('\n')
	indent(level); out.write('<' + node.tag)
	if level == 0:
		out.write(' xmlns:android="http://schemas.android.com/apk/res/android"')
	atts = node.keys()
	if len(atts) > 0:
		if len(atts) > 1: out.write('\n')
		atts.sort(cmp=attcmp)
		for att in atts:
			key = att.replace(NS, ANS)
			val = escape(node.get(att))
			if len(atts) > 1:
				indent(level+1)
				out.write(key+'='+val+'\n')
			else:
				out.write(' '+key+'='+val)
		if len(atts) > 1: indent(level+1)

	if len(node) > 0:
		out.write('>\n')
		for kid in node:
			dumptag(kid, level + 1)
		out.write('\n'); indent(level); out.write('</' + node.tag + '>\n')
	else:
		out.write('/>\n')

for arg in sys.argv[1:]:
	tree = ET.parse(arg).getroot()
	out.write('<?xml version="1.0" encoding="utf-8"?>\n')
	dumptag(tree, 0)
